from django.urls import path
from rest_framework import routers

from .views.category import CategoryViewSet
from .views.customer import CustomerViewSet
from .views.file import FileViewSet
from .views.order import OrderViewSet
from .views.product import ProductViewSet
from .views.store import StoreViewSet
from .views.subscription import SubscriptionViewSet
from .views.user import UserViewSet

# api router
api_router = routers.SimpleRouter()
api_router.register('user', UserViewSet, basename='user')
api_router.register('customer', CustomerViewSet, basename='customer')
api_router.register('store', StoreViewSet, basename='store')
api_router.register('category', CategoryViewSet, basename='category')
api_router.register('file', FileViewSet, basename='file')
api_router.register('product', ProductViewSet, basename='product')
api_router.register('subscription', SubscriptionViewSet, basename='subscription')
api_router.register('order', OrderViewSet, basename='order')
urlpatterns = api_router.urls

