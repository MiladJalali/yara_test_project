from datetime import datetime

from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission

from content.models import File, Product, Subscription
from customers.models import CustomerSubscriptions, Customer
from orders.models import OrderFile, OrderProduct, Order
from stores.models import Store

User = get_user_model()


class IsCustomer(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.role == 1


class IsSelfOrAdmin(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            user = User.objects.get(pk=view.kwargs.get('pk'), is_active=True)
        except:
            return False
        else:
            return request.user == user or request.user.is_staff


class HasDownloadPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False

        try:
            file_id = int(view.kwargs.get('pk'))
        except:
            return False
        else:
            file = File.objects.select_related('product'). \
                select_related('product__store').get(id=file_id, is_active=True)

            # check is free
            if file.price == 0:
                return True

            # check file purchased
            if OrderFile.objects. \
                    select_related('order'). \
                    filter(file_id=file_id,
                           order__customer=request.user,
                           order__is_paid=True).exists():
                return True

            # check product purchased
            elif OrderProduct.objects. \
                    select_related('order'). \
                    filter(order__customer=request.user,
                           order__is_paid=True,
                           product__id=file.product.id).exists():

                return True

            # check subscription
            return CustomerSubscriptions.objects.filter(customer=request.user.customer,
                                                        store=file.product.store,
                                                        expire_time__gte=datetime.now(),
                                                        is_active=True,
                                                        ).exists()


class IsStoreOwnerType(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.role == 2


class IsFileOwner(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            file_id = view.kwargs.get('pk')
            file_owner = File.objects.get(pk=file_id, is_active=True).product.store.owner
        except:
            return False
        return request.user == file_owner


class IsStoreOwner(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            store_owner = Store.objects.get(pk=view.kwargs.get('pk'), is_active=True).owner
        except:
            return False
        return request.user == store_owner


class IsProductOwner(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            product_owner = Product.objects.get(pk=view.kwargs.get('pk'), is_active=True).store.owner
        except:
            return False
        return request.user == product_owner


class IsOrderCustomer(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            customer = Order.objects.get(id=view.kwargs.get('pk'), is_active=True).customer
        except:
            return False
        return request.user == customer


class IsSubscriptionOwner(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            subscription_owner = Subscription.objects.get(id=view.kwargs.get('pk'), is_active=True).store.owner
        except:
            return False
        return request.user == subscription_owner


class IsProductBuyer(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            orders = OrderProduct.objects.filter(product=view.kwargs.get('pk'), is_active=True, order__is_paid=True)
            for order in orders:
                if request.user == order.customer.user:
                    return True
            return False
        except:
            return False


class IsSelf(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        try:
            customer = Customer.objects.filter(id=view.kwargs.get('pk'), is_active=True)
            return request.user.customer == customer
        except:
            return False
