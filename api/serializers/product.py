from rest_framework import serializers

from comments.models import Rate, Comment
from content.models import Category, Product, ProductCategory
from stores.models import Store


class ProductSerializer(serializers.ModelSerializer):
    categories = serializers.ListField(write_only=True)
    store_id = serializers.IntegerField(min_value=1, write_only=True)

    def create(self, validated_data):
        categories = Category.objects.filter(id__in=validated_data['categories'])
        instance = Product.objects.create(name=validated_data['name'],
                                          price=validated_data['price'],
                                          store=Store.objects.get(id=validated_data['store_id']),
                                          )
        for category in categories:
            ProductCategory.objects.create(product=instance, category=category)
        return instance

    def validate_categories(self, values):
        try:
            categories = Category.objects.filter(id__in=values)
            if len(categories) == len(values):
                return values
        except:
            serializers.ValidationError({'message': 'Wrong categories'}, code=400)

    def validate_name(self, value):
        if Product.objects.filter(name=value).exists():
            raise serializers.ValidationError({'message': 'a product with this name is already exist'})
        return value

    class Meta:
        model = Product
        exclude = ('is_active', 'created_time', 'modified_time')


class ProductSearchSerializer(ProductSerializer):
    class Meta(ProductSerializer.Meta):
        pass

    # TODO
    # def validate(self, attrs):
    #     pass


class RateSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        product = validated_data['product']
        rate = Rate.objects.create(product=product,
                                   value=validated_data['value'],
                                   user=self.context['request'].user)
        return rate

    class Meta:
        model = Rate
        exclude = ('id', 'is_active', 'modified_time')
        read_only_fields = ('user',)


class CommentSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        product = validated_data['product']
        comment = Comment.objects.create(product=product,
                                         description=validated_data['description'],
                                         replay_to=validated_data.get('replay_to'),
                                         user=self.context['request'].user,
                                         is_active=False)
        return comment

    class Meta:
        model = Comment
        exclude = ('id', 'is_active', 'modified_time')
        read_only_fields = ('user',)
