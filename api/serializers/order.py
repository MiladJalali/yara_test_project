from django.contrib.auth import get_user_model
from django.db.models import Sum
from rest_framework import serializers

from content.models import File, Product, Subscription
from orders.models import Order, OrderFile, OrderProduct, OrderSubscription

User = get_user_model()


class OrderSerializer(serializers.ModelSerializer):
    files_id = serializers.ListField(required=False)
    products_id = serializers.ListField(required=False)
    subscriptions_id = serializers.ListField(required=False)
    customer = serializers.StringRelatedField()

    def create(self, validated_data):
        total_price = 0
        order = Order.objects.get_or_create(is_active=True, is_paid=False,
                                            customer=self.context['request'].user.customer)
        # add files to order and update order price
        if validated_data.get('files_id'):
            files = File.objects.filter(id__in=validated_data['files_id'])
            total_price += files.aggregate(Sum('price'))['price__sum']
            for file in files:
                OrderFile.objects.create(order=order, file=file)

        # add products to order and update order price
        if validated_data.get('products_id'):
            products = Product.objects.filter(id__in=validated_data['products_id'])
            total_price += products.aggregate(Sum('price'))['price__sum']
            for product in products:
                OrderProduct.objects.create(order=order, product=product)

        # add subscriptions to order and update order price
        if validated_data.get('subscriptions_id'):
            subscriptions = Subscription.objects.filter(id__in=validated_data['subscriptions_id'])
            total_price += subscriptions.aggregate(Sum('price'))['price__sum']
            for subscription in subscriptions:
                OrderSubscription.objects.create(order=order, subscription=subscription)
            order.price = total_price
            order.save()
        return order

    def validate_subscriptions_id(self, values):
        try:
            subscriptions = Subscription.objects.filter(id__in=values).exists()
        except:
            raise serializers.ValidationError(detail='subscriptions not found', code=400)
        # check no repeated subscriptions exist
        if len(set(values)) != len(values):
            raise serializers.ValidationError(detail='duplicate subscriptions', code=400)
        return values

    def validate_products_id(self, values):
        try:
            products = Product.objects.filter(id__in=values).exists()
        except:
            raise serializers.ValidationError(detail='products not found', code=400)

        # check no repeated products exist
        if len(set(values)) != len(values):
            raise serializers.ValidationError(detail='duplicate products', code=400)
        return values

    def validate_files_id(self, values):
        try:
            files = File.objects.filter(id__in=values).exists()
        except:
            raise serializers.ValidationError(detail='files not found', code=400)
        # check no repeated files exist
        if len(set(values)) != len(values):
            raise serializers.ValidationError(detail='duplicate files', code=400)
        return values

    class Meta:
        model = Order
        exclude = ('created_time', 'modified_time', 'is_active',)
        read_only_fields = ('id', 'uuid', 'price', 'store', 'customer', 'is_paid',)
