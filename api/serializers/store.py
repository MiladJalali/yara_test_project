from datetime import datetime, timedelta

from django.db.models import Sum
from rest_framework import serializers

from orders.models import Sale
from stores.models import Store


class StoreSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        store = Store.objects.create(name=validated_data['name'],
                                     owner=self.context['request'].user)
        return store

    class Meta:
        model = Store
        fields = ('id', 'name', 'owner',)
        read_only_fields = ('id', 'owner',)


class SaleSerializer(serializers.Serializer):
    duration = serializers.IntegerField(min_value=1)
    store = serializers.StringRelatedField()
    total_sales = serializers.IntegerField()
    files_sale = serializers.IntegerField()
    products_sale = serializers.IntegerField()
    subscriptions_sale = serializers.IntegerField()

    def create(self, validated_data):
        store = self.context['request'].user.store
        duration = validated_data['duration']
        sales = Sale.objects.filter(store=store, created_time__gte=datetime.now() - timedelta(days=duration))
        total_sales = sales.aggregate(Sum('price'))['price__sum']
        files_sale = sales.exclude(file=None).aggregate(Sum('price'))['price__sum']
        products_sale = sales.exclude(product=None).aggregate(Sum('price'))['price__sum']
        subscriptions_sale = sales.exclude(subscription=None).aggregate(Sum('price'))['price__sum']
        return {
            'store': store,
            'total_sales': total_sales,
            'files_sale': files_sale,
            'products_sale': products_sale,
            'subscriptions_sale': subscriptions_sale,
        }

    class Meta:
        model = Sale
        read_only_fields = ('store', 'total_sales', 'files_sale', 'products_sale', 'subscriptions_sale')


class FileSaleSerializer(serializers.Serializer):
    duration = serializers.IntegerField(min_value=1)
    files_sale = serializers.IntegerField(read_only=True)
    files_id = serializers.ListField()

    def create(self, validated_data):
        store = self.context['request'].user.store
        duration = validated_data['duration']
        sales = Sale.objects.filter(store=store,
                                    file__in=validated_data['files_id'],
                                    created_time__gte=datetime.now() - timedelta(days=duration))
        files_sale = sales.aggregate(Sum('price'))['price__sum']
        return {'files_id': validated_data['files_id'], 'files_sale': files_sale, 'duration': duration}
    # TODO
    # def validate(self, attrs):
    #     pass


class ProductSaleSerializer(serializers.Serializer):
    products_id = serializers.ListField()
    duration = serializers.IntegerField(min_value=1)
    products_sale = serializers.IntegerField(read_only=True)

    def create(self, validated_data):
        store = self.context['request'].user.store
        duration = validated_data['duration']
        sales = Sale.objects.filter(store=store,
                                    product__in=self.validated_data['products_id'],
                                    created_time__gte=datetime.now() - timedelta(days=duration))
        products_sale = sales.aggragete(Sum('price'))['price__sum']
        return {'products_id': validated_data['products_id'],
                'duration': duration,
                'products_sale': products_sale
                }
    # TODO
    # def validate(self, attrs):
    #     pass


class SubscriptionSaleSerializer(serializers.Serializer):
    subscriptions_id = serializers.ListField()
    duration = serializers.IntegerField(min_value=1)
    subscription_sale = serializers.IntegerField(read_only=True)

    def create(self, validated_data):
        store = Store.objects.get(id=validated_data['store'])
        duration = validated_data['duration']
        sales = Sale.objects.filter(store=store,
                                    subscription__in=validated_data['subscription_ids'],
                                    created_time__gte=datetime.now() - timedelta(days=duration))
        subscriptions_sale = sales.aggragete(Sum('price'))['price__sum']
        return {
            'subscriptions_id': validated_data['subscriptions_id'],
            'duration': duration,
            'subscription_sale': subscriptions_sale
        }
    # TODO
    # def validate(self, attrs):
    #     pass
