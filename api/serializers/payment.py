from rest_framework import serializers

from customers.models import Customer, CustomerSubscriptions
from orders.models import Order
from datetime import datetime, timedelta


class PaymentSerializer(serializers.Serializer):
    order_id = serializers.PrimaryKeyRelatedField()

    def create(self, validated_data):
        order = Order.objects.get(id=validated_data['order_id'])
        order.is_paid = True
        order.save()

        # create subscription for user or update expire time
        subscriptions = order.subscriptions.all()
        for subscription in subscriptions:
            store = subscription.store
            customer = Customer.objects.get(id=validated_data.get['customer_id'])
            try:
                customer_subscription = CustomerSubscriptions.objects.get(customer=customer,
                                                                          store=store,
                                                                          is_active=True)
            except CustomerSubscriptions.DoesNotExist:
                CustomerSubscriptions.objects.create(customer=customer,
                                                     store=order.store,
                                                     expire_time=datetime.now() + timedelta(30 * subscription.duration))
            else:
                customer_subscription.expire_time += timedelta(days=30 * subscription.duration)
                customer_subscription.save()
        return {'order_id': order.id}
    # TODO
    # def validate(self, attrs):
    #     pass
