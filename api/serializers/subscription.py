from rest_framework import serializers

from content.models import Subscription


class SubscriptionSerializer(serializers.ModelSerializer):
    store = serializers.StringRelatedField()

    def create(self, validated_data):
        subscription = Subscription.objects.create(store=self.context['request'].user.store, **validated_data)
        return subscription

    def validate_duration(self, value):
        if Subscription.objects.filter(store=self.context['request'].user.store, duration=value):
            raise serializers.ValidationError({'message': 'This subscription duration already exists.'})
        return value

    def validate(self, attrs):
        user = self.context['request'].user
        if self.instance.store.owner != user and not user.is_staff:
            raise serializers.ValidationError(detail='Access denied', code=401)

    class Meta:
        model = Subscription
        fields = ('id', 'store', 'duration', 'price')
