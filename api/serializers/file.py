from rest_framework import serializers

from content.models import File


class FileWriteSerializer(serializers.ModelSerializer):
    is_free = serializers.ChoiceField(choices=[0, 1])
    product = serializers.StringRelatedField()

    def create(self, validated_data):
        if validated_data['is_free'] == 1:
            file = File.objects.create(type=validated_data['type'],
                                       file=validated_data['file'],
                                       price=0,
                                       product=validated_data['product'],
                                       )
            return file
        return File.objects.create(type=validated_data['type'],
                                   file=validated_data['file'],
                                   price=validated_data['price'],
                                   product=validated_data['product'])

    class Meta:
        model = File
        fields = ('id', 'name', 'price', 'product', 'type', 'is_free')


class FileReadSerializer(serializers.ModelSerializer):
    product = serializers.StringRelatedField()

    class Meta:
        model = File
        fields = ('id', 'name', 'price', 'product', 'type')
