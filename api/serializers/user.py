from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from rest_framework import serializers

from customers.models import Customer
from users.models import PasswordResetToken

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    role = serializers.ChoiceField(choices=(2, 3))

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'phone_number', 'password', 'password2', 'role')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
            role=validated_data['role']
        )
        user.set_password(validated_data['password'])
        user.save()
        if validated_data['role'] == 1:
            Customer.objects.create(user=user)
        return user

    def validate(self, attrs):
        if attrs.get('password') != attrs.get('password2'):
            raise serializers.ValidationError('passwords are not match')
        return attrs


class ValidateTokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=22)

    def validate(self, attrs):
        if PasswordResetToken.objects.filter(
                value=attrs['token'],
                is_active=True,
                created_time__gte=datetime.now() - timedelta(days=2)
        ).exists():
            return attrs
        else:
            raise serializers.ValidationError({'token': 'token is expired or invalid'})


class ResetPasswordSerializer(ValidateTokenSerializer):
    password = serializers.CharField(style={'input_type': 'password'})
    password2 = serializers.CharField(style={'input_type': 'password'})

    def create(self, validated_data):
        token = PasswordResetToken.objects.select_related('user').get(value=validated_data['token'])
        token.is_active = False
        token.user.set_password(validated_data['password'])
        token.user.save()
        token.save()
        return 'OK'

    def validate(self, attrs):
        super().validate(attrs)
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError('Passwords are not match.')
        return attrs


class ChangePasswordSerializer(serializers.Serializer):
    password = serializers.CharField(style={'input_type': 'password'})
    password2 = serializers.CharField(style={'input_type': 'password'})

    def validate(self, attrs):
        super().validate(attrs)
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError('Passwords are not match.')
        return attrs


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()
