from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsSubscriptionOwner, IsStoreOwner
from api.serializers.subscription import SubscriptionSerializer
from api.utils import safe_delete
from content.models import Subscription

User = get_user_model()


class SubscriptionViewSet(viewsets.ModelViewSet):
    serializer_class = SubscriptionSerializer
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = Subscription.objects.filter(is_active=True)

    def get_permissions(self):
        permission_classes = []
        if self.action in ['retrieve', 'list', ]:
            permission_classes = [AllowAny, ]
        if self.action == 'create':
            permission_classes = [IsStoreOwner, ]
        if self.action in ['update', 'partial_update', 'destroy']:
            permission_classes = [IsSubscriptionOwner, ]
        return [permission() for permission in permission_classes]

    def destroy(self, request, *args, **kwargs):
        return safe_delete('Subscription', kwargs.get('pk'))
