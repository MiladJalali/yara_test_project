from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import status

from api.serializers.order import OrderSerializer
from api.utils import safe_delete
from orders.models import Order
from api.permissions import IsCustomer, IsSelf


class CustomerViewSet(viewsets.ViewSet):
    permission_classes = (IsCustomer,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'get_orders':
            permission_classes = [IsSelf, ]
        return [permission() for permission in permission_classes]

    @action(methods=['get'], detail=True)
    def get_orders(self, request):
        orders = Order.objects.filter(is_paid=True, customer=self.request.user)
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        return safe_delete('Customer', kwargs.get('pk'))
