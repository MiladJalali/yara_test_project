from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from api.serializers.payment import PaymentSerializer


@api_view(http_method_names=['post'])
def after_pay(request):
    # fake payment implementation for test
    serializer = PaymentSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response({'message': 'successfully paid'}, status=HTTP_200_OK)
