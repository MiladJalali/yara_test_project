import mimetypes

from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsStoreOwnerType, HasDownloadPermission, IsFileOwner
from api.serializers.file import FileReadSerializer, FileWriteSerializer
from api.utils import safe_delete
from content.models import File
from content.tasks import download_file


class FileViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = File.objects.filter(is_active=True)
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve', ]:
            return FileReadSerializer
        return FileWriteSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [IsStoreOwnerType, ]
        elif self.action == 'retrieve':
            permission_classes = [IsFileOwner, ]
        elif self.action in ['create', 'destroy', 'update', 'partial_update']:
            permission_classes = [IsFileOwner, ]
        elif self.action == 'download':
            permission_classes = [HasDownloadPermission, ]
        return [permission() for permission in permission_classes]

    # safe delete file
    def destroy(self, request, *args, **kwargs):
        return safe_delete('File', kwargs.get('pk'))

    @action(methods=['get'], detail=True)
    def download(self, request, pk=None):
        # sync download for test
        file_object = File.objects.get(id=pk)
        file_path = f'media/{file_object.file.name}'
        with open(file_path, 'rb', ) as file:
            content = file.read()
            mime_type, _ = mimetypes.guess_type(file_path)
        return HttpResponse(content=content, content_type=mime_type)

        # Async Download : The client will poll for result every 5 seconds(we can also leverage socket.io to build a long connection here which is more elegent) with the task_id we sent on last request.
        # download_file.delay(pk=pk)
        # return Response({'tas_id': 'download_file.task_id'})
