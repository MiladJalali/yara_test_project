from django.contrib.auth import get_user_model
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsSelfOrAdmin
from api.serializers.user import UserSerializer, ValidateTokenSerializer, ResetPasswordSerializer, \
    ChangePasswordSerializer, EmailSerializer
from api.utils import safe_delete
from users.tasks import send_email

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = UserSerializer
    queryset = User.objects.filter(is_active=True)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [AllowAny, ]
        if self.action in ['update', 'partial_update']:
            permission_classes = [IsSelfOrAdmin, ]
        elif self.action in ['destroy', 'list']:
            permission_classes = [IsAdminUser, ]
        elif self.action in ['retrieve', 'change_password']:
            permission_classes = [IsSelfOrAdmin]
        return [permission() for permission in permission_classes]

    def destroy(self, request, *args, **kwargs):
        return safe_delete('User', pk=kwargs.get('pk'))

    @action(methods=['post'], detail=False)
    def send_reset_password_email(self, request):
        serializer = EmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_email.delay(serializer.data['email'])
        return Response({'message': 'email send'}, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True,)
    def change_password(self, request):
        # TODO code refactor
        serializer = ChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(pk=request.user.id)
        user.set_password(serializer.data['password'])
        user.save()
        return Response({'message': 'password change successfully'}, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=False)
    def check_token(self, request):
        serializer = ValidateTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'status': 'OK'})

    @action(methods=['post'], detail=False)
    def reset_password(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': 'password reset successfully'}, status=status.HTTP_200_OK)
