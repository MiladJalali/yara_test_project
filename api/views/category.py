from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.utils import safe_delete
from content.models import Category


class CategoryViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = Category.objects.filter(is_active=True)

    def get_permissions(self):
        permission_classes = []
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            permission_classes = [IsAdminUser, ]
        elif self.action in ['list', 'retrieve']:
            permission_classes = [AllowAny, ]
        return [permission() for permission in permission_classes]

    def destroy(self, request, *args, **kwargs):
        return safe_delete('Category', kwargs.get('pk'))
