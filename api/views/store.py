from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsStoreOwnerType, IsStoreOwner, IsProductOwner, IsFileOwner, IsSubscriptionOwner
from api.serializers.store import StoreSerializer, SaleSerializer, FileSaleSerializer, ProductSaleSerializer, \
    SubscriptionSaleSerializer
from api.utils import safe_delete
from stores.models import Store


class StoreViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = (StoreSerializer,)
    queryset = Store.objects.filter(is_active=True)

    def get_permissions(self):
        permission_classes = []
        if self.action in ['retrieve', 'list']:
            permission_classes = [AllowAny, ]
        elif self.action == 'create':
            permission_classes = [IsStoreOwnerType, ]
        elif self.action in ['update', 'partial_update', 'get_sales']:
            permission_classes = [IsStoreOwner, ]
        elif self.action == 'destroy':
            permission_classes = [IsAdminUser, ]
        elif self.action == 'get_file_sales':
            permission_classes = [IsFileOwner, ]
        elif self.action == 'get_product_sales':
            permission_classes = [IsProductOwner, ]
        elif self.action == 'get_subscription_sales':
            permission_classes = [IsSubscriptionOwner, ]
        return [permission() for permission in permission_classes]

    # safe delete store
    def destroy(self, request, *args, **kwargs):
        return safe_delete('Store', kwargs.get('pk'))

    # store reports views
    @action(methods=['get'], detail=True)
    def get_sales(self, request):
        serializer = SaleSerializer(data=request.query_params, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=False, )
    def get_file_sales(self, request):
        serializer = FileSaleSerializer(data=request.query_params, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=True, )
    def get_product_sales(self, request):
        serializer = ProductSaleSerializer(data=request.query_params, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=True, )
    def get_subscription_sales(self, request):
        serializer = SubscriptionSaleSerializer(data=request.query_params, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
