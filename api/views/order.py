from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsCustomer, IsOrderCustomer
from api.serializers.order import OrderSerializer
from api.utils import safe_delete
from orders.models import Order


class OrderViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = Order.objects.filter(is_active=True)
    serializer_class = OrderSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [IsCustomer, ]
        if self.action in ['update', 'partial_update']:
            permission_classes = [IsCustomer]
        elif self.action in ['retrieve']:
            permission_classes = [IsOrderCustomer]
        elif self.action == 'list':
            permission_classes = [IsAdminUser, ]
        return [permission() for permission in permission_classes]

    def destroy(self, request, *args, **kwargs):
        return safe_delete('Order', kwargs.get('pk'))
