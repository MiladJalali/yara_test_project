from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.permissions import IsStoreOwnerType, IsProductOwner, IsProductBuyer
from api.serializers.product import ProductSerializer, RateSerializer, CommentSerializer
from api.utils import safe_delete
from comments.models import Comment
from content.models import Product


class ProductViewSet(viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = Product.objects.filter(is_active=True)
    serializer_class = ProductSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action in ['retrieve', 'list']:
            permission_classes = [AllowAny, ]
        elif self.action == 'create':
            permission_classes = [IsStoreOwnerType]
        elif self.action in ['update', 'partial_update']:
            permission_classes = [IsProductOwner]
        elif self.action == 'destroy':
            permission_classes = [IsProductOwner, IsAdminUser]
        elif self.action == 'add_rate':
            permission_classes = [IsProductBuyer, ]
        elif self.action == 'add_comment':
            permission_classes = [IsAuthenticated, ]

        return [permission() for permission in permission_classes]

    # safe delete product (set is_active to False)
    def destroy(self, request, *args, **kwargs):
        return safe_delete('Product', kwargs.get('pk'))

    @action(methods=['get'], detail=False)
    def search(self, request):
        # TODO add a serializer to validate q
        products = Product.objects.get_products(request.query_params.get('categories'),
                                                request.query_params.get('type'),
                                                request.query_params.get('ordering'))
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=False)
    def add_rate(self, request):
        serializer = RateSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=False)
    def add_comment(self, request):
        serializer = CommentSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=True)
    def get_comments(self, request, pk=None):
        try:
            comments = Comment.objects.filter(product_id=pk, is_active=True)
        except:
            return Response({'message': 'product not found'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = CommentSerializer(comments, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
