from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response

from customers.models import Customer
from comments.models import Comment, Rate
from orders.models import Order
from stores.models import Store
from payments.models import Invoice


def safe_delete(object_type, pk):
    try:
        if object_type == 'User':
            User = get_user_model()
            object = User.objects.get(pk=pk)
        else:
            object = eval(object_type).objects.get(pk=pk)
    except:
        return Response({'message': f'{object_type} not found'}, status=status.HTTP_404_NOT_FOUND)
    else:
        object.is_active = False
        object.save()
        return Response({'message': f'{object_type} deleted successfully'}, status=status.HTTP_200_OK)
