from uuid import uuid4

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from content.models import Subscription, Product, File
from core.models import BaseModel
from customers.models import Customer
from stores.models import Store

User = get_user_model()


# This order model use to report to customers
class Order(BaseModel):
    # Relations
    uuid = models.UUIDField(default=uuid4, unique=True, verbose_name=_('uuid'))
    customer = models.ForeignKey(to=User,
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 related_name='orders',
                                 verbose_name=_('customer'))
    files = models.ManyToManyField(to=File, through='OrderFile')
    products = models.ManyToManyField(to=Product, through='OrderProduct')
    subscriptions = models.ManyToManyField(to=Subscription, through='OrderSubscription')

    # Fields
    is_paid = models.BooleanField(default=False, verbose_name=_('is paid'))
    price = models.PositiveIntegerField(default=0, verbose_name=_('price'))

    class Meta:
        db_table = 'order'
        verbose_name = _('order')
        verbose_name_plural = _('orders')

    def __str__(self):
        return str(self.id)


class OrderFile(BaseModel):
    order = models.ForeignKey(to=Order,
                              on_delete=models.SET_NULL,
                              null=True,
                              verbose_name=_('order'))
    file = models.ForeignKey(to=File,
                             on_delete=models.SET_NULL,
                             null=True,
                             verbose_name=_('file'))

    class Meta:
        db_table = 'order_file'
        verbose_name = _('order file')
        verbose_name_plural = _('order files')

    def __str__(self):
        return str(self.id)


class OrderProduct(BaseModel):
    order = models.ForeignKey(to=Order,
                              on_delete=models.SET_NULL,
                              null=True,
                              verbose_name=_('order'))

    product = models.ForeignKey(to=Product, on_delete=models.SET_NULL,
                                null=True,
                                verbose_name=_('product'))

    class Meta:
        db_table = 'order_product'
        verbose_name = _('order product')
        verbose_name_plural = _('order products')

    def __str__(self):
        return str(self.id)


class OrderSubscription(BaseModel):
    order = models.ForeignKey(to=Order,
                              on_delete=models.SET_NULL,
                              null=True,
                              verbose_name=_('order'))
    subscription = models.ForeignKey(to=Subscription, on_delete=models.SET_NULL,
                                     null=True,
                                     verbose_name=_('subscription'))

    class Meta:
        db_table = 'order_subscription'
        verbose_name = _('order subscription')
        verbose_name_plural = _('order subscriptions')

    def __str__(self):
        return str(self.id)


# This model use to store reports
# This model can divide to 3 model to prevent null values, but in this model we can get reports in 1 database hit
class Sale(BaseModel):
    # Relations
    order = models.ForeignKey(to=Order,
                              on_delete=models.SET_NULL,
                              null=True,
                              related_name='sales',
                              verbose_name=_('order'))

    file = models.ForeignKey(to=File,
                             on_delete=models.SET_NULL,
                             null=True,
                             blank=True,
                             related_name='sales',
                             verbose_name=_('file'))

    product = models.ForeignKey(to=Product,
                                on_delete=models.SET_NULL,
                                null=True,
                                blank=True,
                                related_name='sales',
                                verbose_name=_('product'))

    subscription = models.ForeignKey(to=Subscription,
                                     on_delete=models.SET_NULL,
                                     null=True,
                                     blank=True,
                                     related_name='sales',
                                     verbose_name=_('product'))

    store = models.ForeignKey(to=Store,
                              on_delete=models.SET_NULL,
                              null=True,
                              related_name='sales',
                              verbose_name=_('store'))
    # Fields
    price = models.PositiveIntegerField()

    class Meta:
        db_table = 'sale'
        verbose_name = _('sale')
        verbose_name_plural = _('sales')

    def __str__(self):
        return str(self.id)
