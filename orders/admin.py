from django.contrib import admin

from orders.models import Order, OrderFile, OrderProduct, OrderSubscription, Sale


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderFile)
class OrderFileAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderProduct)
class OrderProductAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderSubscription)
class OrderSubscriptionAdmin(admin.ModelAdmin):
    pass


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass
