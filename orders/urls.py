from django.urls import path
from rest_framework import routers

from . import views

app_name = 'order'
urlpatterns = [
    path('download_free_file/<str:file_name>', views.download_free_file, name='download-free-file')
]

# order router
order_router = routers.SimpleRouter()
order_router.register('', views.OrderViewSet, basename='order')
urlpatterns += order_router.urls
