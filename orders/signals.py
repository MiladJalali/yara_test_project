# from django.db.models.signals import post_save
# from django.dispatch import receiver
#
# from orders.models import Order, Sale
#
#
# @receiver(post_save, sender=Order)
# def add_sale(sender, instance, **kwargs):
#     if instance.is_paid:
#         for file in instance.files.all():
#             Sale.objects.create(file=file, store=file.product.store, price=file.price)
#
#         for product in instance.products.all():
#             Sale.objects.create(product=product, store=product.store, price=product.price)
#
#         for subscription in instance.subscriptions.all():
#             Sale.objects.create(subscription=subscription, store=subscription.store, price=subscription.price)
