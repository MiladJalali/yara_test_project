from django.db import models
from django.utils.translation import ugettext_lazy as _

from content.managers import ProductManager
from core.models import BaseModel
from stores.models import Store


class File(BaseModel):
    AUDIO = 1
    VIDEO = 2
    PDF = 3
    TYPE = (
        (AUDIO, _('Audio')),
        (VIDEO, _('Video')),
        (PDF, _('PDF')),
    )

    # Relations
    product = models.ForeignKey(to='Product',
                                on_delete=models.CASCADE,
                                related_name='files',
                                verbose_name=_('product'))

    # Fields
    type = models.PositiveSmallIntegerField(choices=TYPE, verbose_name=_('type'))
    file = models.FileField(upload_to=f'files/',
                            verbose_name=_('file'))
    name = models.CharField(max_length=100)
    price = models.PositiveIntegerField(verbose_name=_('price'))

    class Meta:
        db_table = 'file'
        verbose_name = _('file')
        verbose_name_plural = _('files')

    def __str__(self):
        return self.file.name


class Product(BaseModel):
    # Relations
    store = models.ForeignKey(to=Store,
                              on_delete=models.CASCADE,
                              related_name='products',
                              verbose_name=_('store'))
    categories = models.ManyToManyField(to='Category',
                                        through='ProductCategory',
                                        related_name='products')

    # Fields
    name = models.CharField(max_length=100, verbose_name=_('name'))
    price = models.PositiveIntegerField(verbose_name=_('price'))
    description = models.TextField()

    objects = ProductManager()

    class Meta:
        db_table = 'product'
        verbose_name = _('product')
        verbose_name_plural = _('products')

    def __str__(self):
        return self.name


class Category(BaseModel):
    name = models.CharField(max_length=50, unique=True, verbose_name=_('category'))

    class Meta:
        db_table = 'category'
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


class ProductCategory(BaseModel):
    product = models.ForeignKey(to=Product,
                                on_delete=models.CASCADE,
                                verbose_name=_('product')
                                )

    category = models.ForeignKey(to=Category,
                                 on_delete=models.CASCADE,
                                 verbose_name=_('category')
                                 )

    class Meta:
        db_table = 'product_category'
        verbose_name = _('product category')
        verbose_name_plural = _('product categories')

    def __str__(self):
        return str(self.id)


class Subscription(BaseModel):
    ONE_MONTH = 1
    THREE_MONTH = 3
    SIX_MONTH = 6
    TWENTY_MONTH = 12
    TYPE = (
        (ONE_MONTH, _('1 Month')),
        (THREE_MONTH, _('3 Month')),
        (SIX_MONTH, _('6 Month')),
        (TWENTY_MONTH, _('12 month')),
    )

    # Relations
    store = models.ForeignKey(to=Store,
                              on_delete=models.CASCADE,
                              related_name='subscriptions',
                              verbose_name=_('store')
                              )

    # Fields
    duration = models.PositiveSmallIntegerField(choices=TYPE, verbose_name=_('duration'))
    price = models.PositiveIntegerField(verbose_name=_('price'))

    class Meta:
        db_table = 'subscription'
        verbose_name = _('subscription')
        verbose_name_plural = _('subscriptions')

    def __str__(self):
        return str(self.id)
