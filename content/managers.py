from django.db import models
from django.db.models import Avg


class ProductManager(models.Manager):

    def get_products(self, categories, type, ordering):
        self.queryset = super().get_queryset()
        if categories:
            self.filter_by_categories(categories)
        if type:
            self.filter_by_type(type)
        if ordering:
            self.sort(ordering)
        return self.queryset

    def sort(self, ordering):
        if ordering == 'newest':
            self.queryset = self.queryset.order_by('-created_time')
        if ordering == 'rate':
            self.queryset = self.queryset.annotate(average_rate=Avg('rates__value')).order_by('average_rate').order_by(
                '-average_rate')

    def filter_by_type(self, type):
        if type == 'free':
            self.queryset = self.queryset.filter(price=0)
        if type == 'not_free':
            self.queryset = self.queryset.filter(price__gt=0)

    def filter_by_categories(self, categories):
        self.queryset = self.queryset.filter(categories__in=[categories])
