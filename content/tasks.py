import mimetypes

from celery.task import task
from django.http import HttpResponse

from content.models import File


@task(name='download_file')
def download_file(pk):
    file_object = File.objects.get(id=pk)
    file_path = f'media/{file_object.file.name}'
    with open(file_path, 'rb', ) as file:
        content = file.read()
        mime_type, _ = mimetypes.guess_type(file_path)
    return HttpResponse(content=content, content_type=mime_type)
