from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel

User = get_user_model()


class Store(BaseModel):
    # Relations
    owner = models.OneToOneField(to=User,
                                 on_delete=models.CASCADE,
                                 related_name='store',
                                 verbose_name=_('owner'))

    # Fields
    name = models.CharField(max_length=50, unique=True, verbose_name=_('name'))

    class Meta:
        db_table = 'store'

    def __str__(self):
        return self.name



