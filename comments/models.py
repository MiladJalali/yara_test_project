from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from content.models import Product
from core.models import BaseModel

User = get_user_model()


class Comment(BaseModel):
    # Relations
    user = models.ForeignKey(to=User,
                             on_delete=models.CASCADE,
                             related_name='comments',
                             verbose_name=_('user'))

    product = models.ForeignKey(to=Product,
                                on_delete=models.CASCADE,
                                related_name='comments',
                                verbose_name=_('product'))

    replay_to = models.ForeignKey(to='self',
                                  on_delete=models.CASCADE,
                                  blank=True,
                                  null=True,
                                  related_name='replies',
                                  verbose_name=_('replay_to'))
    # Fields
    description = models.TextField(blank=False)

    class Meta:
        db_table = 'comments'
        verbose_name = _('comments')
        verbose_name_plural = _('comments')

    def __str__(self):
        return str(self.id)


class Rate(BaseModel):
    # Relations
    user = models.ForeignKey(to=User,
                             on_delete=models.CASCADE,
                             related_name='rates',
                             verbose_name=_('user'))

    product = models.ForeignKey(to=Product,
                                on_delete=models.CASCADE,
                                related_name='rates',
                                verbose_name=_('product'))
    # fields
    value = models.FloatField()

    class Meta:
        db_table = 'rate'
        verbose_name = _('rate')
        verbose_name_plural = _('rates')

    def __str__(self):
        return str(self.id)
