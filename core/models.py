from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=_('created_time'))
    modified_time = models.DateTimeField(auto_now_add=True, verbose_name=_('modified_time'))
    # field for safe delete all records in database
    is_active = models.BooleanField(default=True, verbose_name=_('is_active'))

    class Meta:
        abstract = True
