from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel
from stores.models import Store

User = get_user_model()


class Customer(BaseModel):
    # Relations
    user = models.OneToOneField(to=User,
                                on_delete=models.CASCADE,
                                related_name='customer',
                                verbose_name=_('user')
                                )

    subscriptions = models.ManyToManyField(to=Store, through='CustomerSubscriptions', related_name='customers')

    # Extra Fields like credit can be added to this model in the future

    class Meta:
        db_table = 'customer'
        verbose_name = _('customer')
        verbose_name_plural = _('customers')

    def __str__(self):
        return str(self.id)


class CustomerSubscriptions(BaseModel):
    # Relations
    customer = models.ForeignKey(to=Customer,
                                 on_delete=models.CASCADE,
                                 verbose_name=_('customer'))
    store = models.ForeignKey(to=Store,
                              on_delete=models.CASCADE,
                              related_name='customer',
                              verbose_name=_('store'))

    # Fields
    expire_time = models.DateTimeField()

    class Meta:
        db_table = 'customer_subscriptions'
        verbose_name = _('customer_subscriptions')
        verbose_name_plural = _('customers_subscriptions')

    def __str__(self):
        return str(self.id)
