from django.contrib import admin

from customers.models import Customer, CustomerSubscriptions


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass


@admin.register(CustomerSubscriptions)
class CustomerSubscriptionsAdmin(admin.ModelAdmin):
    pass
