from django.urls import path
from rest_framework import routers
from . import views

app_name = 'customers'
urlpatterns = [

]

# customer router
customer_router = routers.SimpleRouter()
customer_router.register('', views.CustomerProfileViewSet, basename='customer')
urlpatterns += customer_router.urls
