from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel


class User(AbstractUser):
    ADMIN = 1
    CUSTOMER = 2
    STORE_OWNER = 3

    TYPE = (
        (ADMIN, _('Admin')),
        (CUSTOMER, _('Customer')),
        (STORE_OWNER, _('Store owner'))
    )

    phone_regex = RegexValidator(regex=r'(\+98|0)?9\d{9}',
                                 message=_("Phone number must be entered in the format: '+989999999999' "
                                           "or '09999999999'. Up to 11 digits allowed."))
    phone_number = models.CharField(validators=[phone_regex],
                                    max_length=11,
                                    unique=True,
                                    null=True,
                                    verbose_name=_('phone number'))

    role = models.PositiveSmallIntegerField(default=2, choices=TYPE, verbose_name=_('role'))
    email = models.EmailField(verbose_name=_('email address'), unique=True)

    class Meta:
        db_table = 'user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username


class PasswordResetToken(BaseModel):
    # Relations
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='password_reset_tokens')

    # Fields
    value = models.CharField(max_length=22)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'password_reset_token'
        verbose_name = _('password_reset_token')
        verbose_name_plural = _('password_reset_tokens')

    def __str__(self):
        return str(self.id)
