from secrets import token_urlsafe

from celery.task import task
from django.conf.global_settings import DEFAULT_FROM_EMAIL
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.template import loader

from users.models import PasswordResetToken

User = get_user_model()


@task(name='send_reset_password_email')
def send_email(email):
    associated_user = User.objects.get(email=email)
    if associated_user:
        token_string = token_urlsafe(16)
        PasswordResetToken.objects.create(user=associated_user, value=token_string)
        c = {
            'username': associated_user.username,
            'token': token_string,
        }
        subject_template_name = 'password_reset_subject.txt'
        email_template_name = 'password_reset_email.html'
        subject = loader.render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        send_mail(subject, email, DEFAULT_FROM_EMAIL, [associated_user.email], fail_silently=False)
