from django.contrib import admin

from payments.models import Invoice


@admin.register(Invoice)
class UserAdmin(admin.ModelAdmin):
    pass
