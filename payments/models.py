from django.db import models

from core.models import BaseModel
from orders.models import Order
from django.utils.translation import ugettext_lazy as _


class Invoice(BaseModel):
    # Relations
    order = models.OneToOneField(to=Order,
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 related_name='payments',
                                 verbose_name=_('order'))

    # Fields
    Reference_Id = models.PositiveIntegerField()

    # price and is_paid in this model is not equal to price and is_paid in order model
    price = models.PositiveIntegerField()
    is_paid = models.BooleanField()

    # Extra Fields can be added in the future
    class Meta:
        db_table = 'payments'
        verbose_name = _('payments')
        verbose_name_plural = _('payments')

    def __str__(self):
        return self.Reference_Id
