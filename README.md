# Yara Test Project


## Step 1:

Create a local_config.py like this:

```python
DATABASE_CONFIG = {
    'ENGINE': 'Your database engine',
    'NAME': 'Your database name',
    'USER': 'Your database user',
    'PASSWORD': 'Your database password',
    'HOST': 'Your database host',
    'PORT': 'Your database port'
}
SECRET = 'your secret key'

HOST_USER = 'Your email host'
HOST_PASSWORD = 'Your password'

```

##Step 2:
Run command migrate
```shell script
python manage.py migrate
```

##Step 3:
run celery worker with this command
```shell script
celery -A YARA_PROJECT worker -l info
```
